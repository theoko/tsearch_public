<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '786741364807601',
        'client_secret' => '1b5c1d107dab4d76fde0b9e09d2101d4',
        'redirect' => 'https://tsearch.eu/auth/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'K2vLmATapOEondS7duDir90dR',
        'client_secret' => 'bL6B5nbhkxZ7xbp0npIdrPPtboqsftIxzvQebXIn8lV9wQFfph',
        'redirect' => 'https://tsearch.eu/auth/twitter/callback',
    ],

    'github' => [
        'client_id' => '2bda01d7c36e12da3fca',
        'client_secret' => '6f16013815770eb065675e7d4a550e661d32e059',
        'redirect' => 'https://tsearch.eu/auth/github/callback',
    ],

];
