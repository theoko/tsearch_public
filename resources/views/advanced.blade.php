@extends('layouts.app')

@section('title', 'Advanced Search')

@section('content')
<style>
    @media only screen and (max-width: 480px){
    .form input[type=text] {
      display:inline-block;

    }
  }
</style>
<div class="flash-message">
@foreach (['danger', 'warning', 'success', 'info'] as $msg)
  @if(Session::has('alert-' . $msg))
  <script type="text/javascript">
  	Materialize.toast('{{ Session::get('alert-' . $msg) }}', 5000);
  </script>
  @endif
@endforeach
</div>
    <div class="container" style="margin-top:100px">
      @if(isset($message))
        <h1 class="flow-text">{{ $message }}</h1>
      @endif
        <form action="/search/{{ $category }}" method="GET">
            <div class="row">
              {!! csrf_field() !!}
            <div class="input-field col s6 m12">
              <input id="input_text" placeholder="Type a keyword" type="text" style="width:50%" name="q" value="@if(isset($query)){{ $query }}@endif" required>
              <label for="input_text">Search</label>
            </div>
            <div class="input-field col s3">
              <input type="date" class="datepicker" id="select_date_from" name="from" value="@if(isset($date_from)){{ $date_from }}@endif">
              <label for="select_date_from">From</label>
            </div>
            <div class="input-field col s3">
              <input type="date" class="datepicker" id="select_date_to" name="to" value="@if(isset($date_to)){{ $date_to }}@endif">
              <label for="select_date_to">To</label>
            </div>
            <div class="input-field col s12"></div>
            <div class="input-field col s4">
              <select id="website_type" name="website_type">
                <option selected disabled>Select a category</option>
                @if(isset($website_type))
                  @foreach($categories as $cat)
                    <option @if($cat == $website_type){{ 'selected' }}@endif>{{ $cat }}</option>
                  @endforeach
                @else
                  @foreach($categories as $cat)
                    <option>{{ $cat }}</option>
                  @endforeach
                @endif
              </select>
              <label>Website Type</label>
            </div>
            <div class="input-field col s12"></div>
            <div class="input-field col s2">
              <button id="submit" type="submit" class="waves-effect waves-light btn blue lighten-2">SEARCH <i class="material-icons right">search</i></button>
            </div>
            </div>
        </form>
    </div>

  @if(isset($results_main_count))
    @if($results_main_count > 0)
    <div class="container">
    <div class="row">
      <div class="col s12 m6" style="width: 100%">
        <div class="card yellow lighten-4">
          <div class="card-content">
            <nav>
            <div class="nav-wrapper blue lighten-2">
              <div class="col s12">
                <a href="https://tsearch.eu/search/all?q={!! $query !!}" class="breadcrumb">{{ $query }}</a>
                @if(!empty($date_from) && !empty($date_to))
                  <a href="https://tsearch.eu/search/advanced?q={!! $query !!}&from={!! $date_from !!}&to={!! $date_to !!}" class="breadcrumb">{{ $date_from }} to {{ $date_to }}</a>
                @endif
                <a href="https://tsearch.eu/search/advanced?q={!! $query !!}&website_type={!! $website_type !!}" class="breadcrumb">{{ $website_type }}</a>
                <a href="#" class="breadcrumb">{{ $results_main_count }} results</a>
              </div>
            </div>
          </nav>
            <p>
            @foreach($results as $data)
            <div class="row">
              <div class="col s12 m6" style="width:100%">
                <div class="card yellow lighten-5">
                  <div class="card-content">
            @if(empty(trim($data->title)))
              <p><a href="{{ $data->link }}">{{ $data->link }}</a></p>
            @else
              <p><a href="{{ $data->link }}">{{ $data->title }}</a></p>
            @endif
            @if(empty(trim($data->description)) || empty(trim($data->header_array)))
              <br><p><br>No description was found.</p>
            @else
              @if(!empty(trim($data->description)))
                  <br><p>(Characters: {{ $data->weight }}) {{ substr($data->description, 0, strpos($data->description, '.', strpos($data->description, '.')+1)) }}</p>
              @else if(!empty(trim($data->header_array)))
                  <br><p>(Characters: {{ $data->weight }}) {{ substr($data->header_array, 0, strpos($data->header_array, '.', strpos($data->header_array, '.')+1)) }}</p>
              @endif
            @endif
              </div>
            </div>
          </div>
          </div>
            @endforeach
            <div align="center">
            {{ $results->appends(Request::except('page'))->links() }}
          </div>
          </p>
        </div>
      </div>
    </div>
    </div>
  </div>
  @endif
@endif

<script>
  $(document).ready(function(){
    $('select').material_select();
    $('.datepicker').pickadate({
    selectMonths: true,
    selectYears: 1,
    closeOnSelect: true,
    format: 'mm/dd/yyyy'
  });
    if($('#input_text').val() == '') {
      $('#input_text').focus();
    }
    $('#input_text').trigger('autoresize');
  });
</script>

@endsection
