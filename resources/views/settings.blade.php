@extends('layouts.app')

@section('title', 'Settings')

@section('content')
<div class="container" style="margin-top:100px">
    <div class="switch">
        <p class="flow-text">Safe Search</p>
    <label>
      Off
      <input type="checkbox">
      <span class="lever"></span>
      On
    </label>
            
  </div>
        <p>
      <input type="checkbox" class="filled-in" id="filled-in-box" />
      <label for="filled-in-box">Enable strict filtering</label>
    </p>
    
    <br>
    <p class="flow-text">Choose Theme</p>
    <div class="col s2">
      <label>Choose Theme</label>
      <select class="browser-default" style="max-width:60%">
    <option value="" disabled selected>Choose preferred theme</option>
    <option value="1">Classic</option>
    <option value="2">Linux / Command Line</option>
  </select>
    </div>
    
    <br>
    <button class="btn waves-effect waves-light blue lighten-1" type="submit" name="action">Save
    <i class="material-icons right">cloud</i>
  </button>
      
</div>
@endsection
