<!--

tSearch cannot/will not replace Google! It is just a small search engine. If you have any suggestions, send me an email. See /contact for more details

-->

<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>tSearch - @yield('title')</title>

  <!-- jQuery -->
  <script src="//static3.tsearch.eu/api/files/js/jquery.js"></script>

  <!-- Compiled and minified CSS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
 <!-- Compiled and minified JavaScript -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

	<!-- Custom CSS -->
	<link rel="stylesheet" href="/frontend/v0.1/css/custom.css" media="screen">

	<!-- Hreflang -->
	<link rel="alternate" href="{{ route('home') }}" hreflang="en-GB" />

  	<style type="text/css">
    body {
       display: flex;
       min-height: 100vh;
       flex-direction: column;
     }

     main {
       flex: 1 0 auto;
     }

		 i.icon-red {
		    color: #f44336;
		}

  	</style>

		<script src="/frontend/v0.1/js/plugins/js.cookie.js"></script>

    <script type="text/javascript">
        window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"light-bottom"};
    </script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
    <!-- End Cookie Consent plugin -->

		<!-- CHART -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

		<script type="text/javascript">
		function getRandomColor() {
				var letters = '0123456789ABCDEF'.split('');
				var color = '#';
				for (var i = 0; i < 6; i++ ) {
						color += letters[Math.floor(Math.random() * 16)];
				}
				return color;
			}
		</script>

		<script type="text/javascript">
			$(document).ready(function() {
				var window_width = $( window ).width();
				var document_width = $( document ).width();
				if (window_width < 1800) {
					$('#desktop_nav').hide();
					$(".button-collapse").sideNav();
					$(".button-collapse").show();
				} else {
					$(".button-collapse").remove();
				}

				if(Cookies.get('dev_consent') == 'true') {
                    $("#card-alert").hide();
					$("#card-alert").remove();
				}

				$(".close-alert").click(function() {
					Cookies.set('dev_consent', 'true', { expires: 2 });
					$("#card-alert").fadeOut();
				});
			});
		</script>

    </head>
    <body>
				<!--<div id="card-alert" class="card orange lighten-5">
					<div class="card-content orange-text">
						<p>While tSearch is under development, the search results may be limited... Consider searching the <a href="https://archive.tsearch.eu/">archive</a></p>
					</div>
					<button type="button" class="close orange-text" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true" class="close-alert">×</span>
					</button>
				</div>-->
      <nav class="white" style="margin-top:-20px;">
      <footer class="page-footer deep-orange accent-1">
        <div class="footer-copyright white">
					<div class="container" id="mobile_nav">
						<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons icon-red">menu</i></a>
                                                 @if(Auth::user())
                                                  <ul id="my_account" class="dropdown-content">
                                                    <li><a href="{{ url('/logout') }}">Logout</a></li>
                                                  </ul>
                                                 @endif
						<ul id="slide-out" class="side-nav">
					    <li><a class="subheader"><img src="https://static3.tsearch.eu/api/files/uploads/jaFVC/Y2LEq0cL4SxGii75i0ej.png" alt="tSearch" class="responsive-img" style="width: 80%"></a></li>
							<li><div class="divider"></div></li>
					    <li><a class="waves-effect" href="{{ route('about') }}">About</a></li>
							<li><a class="waves-effect" href="{{ route('home') }}">Try a search</a></li>
							<li><a class="waves-effect" href="{{ route('statistics') }}">Search insights</a></li>
							<li><a class="waves-effect" href="https://api.tsearch.eu">Developer API</a></li>
							<li><a class="waves-effect" href="https://url.tsearch.eu">Shorten URL</a></li>
                          	<li><a class="waves-effect" href="{{ route('report') }}">Report an issue</a></li>
							<li><a class="waves-effect" href="{{ route('contact') }}">Contact</a></li>
                                                        <li><div class="divider"></div></li>
                                                        @if(Auth::user())
                                                            <li><a class="dropdown-button waves-effect waves-light btn" href="#account-overview" data-activates="my_account">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                                                        @else
                                                            <li><a class="waves-effect waves-light btn" href="{{ url('/login') }}">Sign in <i class="fa fa-btn fa-sign-in"></i></a></li>
                                                        @endif
							<li><div class="divider"></div></li>
							@if(Request::url() != route('home'))
								<li><a class="waves-effect waves-light btn-large blue lighten-2" href="{{ route('advanced') }}">Advanced <i class="material-icons right">search</i></a></li>
								<li><a class="waves-effect waves-light btn-large blue lighten-2" href="{{ route('settings') }}">Settings <i class="material-icons right">settings</i></a></li>
							@else
								<li><a class="waves-effect waves-light btn-large orange lighten-2" href="{{ route('submit') }}">Submit a site <i class="material-icons right">library_add</i></a></li>
							@endif
					  </ul>
					</div>
          <div class="container" id="desktop_nav">
            <a href="{{ route('about') }}" class="blue-text text-lighten-3">
              ABOUT
            </a>
            &nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
						<a href="{{ route('home') }}" class="blue-text text-lighten-3">
              TRY A SEARCH
            </a>
            &nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
						<a href="{{ route('statistics') }}" class="blue-text text-lighten-3">
              SEARCH INSIGHTS
            </a>
						&nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="{{ route('api.home') }}" class="blue-text text-lighten-3">
              API
            </a>
						&nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="{{ route('url.shortener.home') }}" class="blue-text text-lighten-3">
              URL SHORTENER
            </a>
						&nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="{{ route('report') }}" class="blue-text text-lighten-3">
              REPORT AN ISSUE
            </a>
            			&nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="{{ route('contact') }}" class="blue-text text-lighten-3">
              CONTACT
            </a>
                           &nbsp;
                &nbsp;
                &nbsp;
                &nbsp;
            @if(Auth::user())
                <a href="/" class="waves-effect waves-light btn-large">
                    {{ Auth::user()->name }}
                </a>
            @else
                @if(Request::url() != url('/login'))
                    <a href="{{ url('/login') }}" class="waves-effect waves-light btn-large">
                        <i class="fa fa-btn fa-sign-in"></i> Sign In
                    </a>
                @endif
            @endif
            <div class="right">
							@if(Request::url() != route('home'))
								<a class="waves-effect waves-light btn-large blue lighten-2" href="{{ route('advanced') }}">ADVANCED <i class="material-icons right">search</i></a>
								<a class="waves-effect waves-light btn-large blue lighten-2" href="{{ route('settings') }}">SETTINGS <i class="material-icons right">settings</i></a>
							@else
              	<a class="waves-effect waves-light btn-large orange lighten-2" href="{{ route('submit') }}">SUBMIT A SITE <i class="material-icons right">library_add</i></a>
							@endif
            </div>
          </div>
        </div>
      </footer>
    </nav>

<!--

tSearch cannot/will not replace Google! It is just a small search engine. If you have any suggestions, send me an email. See /contact for more details

-->

    @yield('content')

    <script>
        $(".dropdown-button").dropdown();
    </script>

                    <!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"cDE0o1QolK10L7", domain:"tsearch.eu",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=cDE0o1QolK10L7" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-66171111-1', 'auto');
    ga('send', 'pageview');

</script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-6068289084542467",
        enable_page_level_ads: true
    });
</script>

  </body>
  </html>

<!--

tSearch cannot/will not replace Google! It is just a small search engine. If you have any suggestions, send me an email. See /contact for more details

-->
