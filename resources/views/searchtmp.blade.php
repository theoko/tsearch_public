@extends('layouts.app')

@section('title', 'Search')

@section('content')
<style>
    @media only screen and (max-width: 480px){
    .form input[type=text] {
      display:inline-block;

    }
  }
</style>
    <div class="container">
      <br><br><br>
      @if(isset($message))
        <h1 class="flow-text">{{ $message }}</h1>
      @endif
      <br><br><br><br><br>
          <div class="row" class="form">
            <form method="get" action="/search/{{ $category }}" class="col s12">
                <div class="row" style="margin-top: -55px">
                  <div class="input-field col s11" style="margin-top: -10px" align="center">
                            <input id="input_text" type="text" name="q">
                            <label for="input_text">Search</label>
                    </div>
                      <button type="submit" class="waves-effect waves-light btn right" id="call_back_btn" name="submit" value="Search" style="float:right;"><i class="fa fa-search"></i></button>
                  </div>
          </form>
        </div>
    </div>
  </div>

<script>
  $(document).ready(function(){
    $('#input_text').focus();
    $('#input_text').trigger('autoresize');
  });
</script>

@endsection
