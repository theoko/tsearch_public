@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <div class="row" style="width:100%;">
    <!-- <div class="container" style="margin-top: 50px">
      <h1 class="flow-text" style="text-align: center">tSearch is currently under development. However, you can still use all available features</h1>
    </div> -->
    <div class="col s12 m6">
    <div class="card card-block">
    <div align="center" id="form" style="
    		    width: 550px;
    		    height: 400px;
            position: fixed;
    		    top: 0;
    		    bottom: 0;
    		    left: 0;
    		    right: 0;
    		    margin: auto;">
    	  <div id="header" align="center"><h1 class="flow-text" id="text_logo"><img src="https://static3.tsearch.eu/api/files/uploads/jaFVC/Y2LEq0cL4SxGii75i0ej.png" alt="tSearch" class="responsive-img" style="align: center; width: 50%; height: 50%;"></h1>
          <br>
          <div class="row">
            <form method="get" action="https://archive.tsearch.eu/search.php" class="col s12">
                <div class="row">
                  {!! csrf_field() !!}
                  <div class="input-field col s10" style="margin-top: -10px" id="input_text_div">
                            <input id="input_text" type="text" placeholder="Type a keyword" name="q">
                            <label for="input_text">Web Search</label>
                    </div>
                      <button type="submit" class="waves-effect waves-light btn" id="call_back_btn" name="submit" value="Search" style="float:right;"><i class="fa fa-search"></i></button>
                  </div>
          </form>
              <a class="waves-effect waves-light btn light-blue lighten-2" href="{{ route('search') }}" id="cat_search_all">WEB</a>
              <a class="waves-effect waves-light btn cyan lighten-2" href="{{ route('search.images') }}" id="cat_search_images">IMAGES</a>
              <a class="waves-effect waves-light btn red lighten-2" href="{{ route('search.videos') }}" id="cat_search_videos">VIDEOS</a>
              <a class="waves-effect waves-light btn lime lighten-2" href="{{ route('search.news') }}" id="cat_search_news">NEWS</a>
              <a class="waves-effect waves-light btn-large blue lighten-2" href="{{ route('advanced') }}" style="margin-top: 15px">ADVANCED <i class="material-icons right">search</i></a>
              <a class="waves-effect waves-light btn-large blue lighten-2" href="{{ route('settings') }}" style="margin-top: 15px">SETTINGS <i class="material-icons right">settings</i></a>
              <p class="flow-text" style="font-size: 140%;margin-top:10px">Suggestions</p>
              <p class="flow-text" style="font-size: 130%"><a href="https://archive.tsearch.eu/search.php?q=austria">Try "Austria"</a></p>
              <p class="flow-text" style="font-size: 130%"><a href="https://archive.tsearch.eu/search.php?q=python">Try "Python"</a></p>
              <p class="flow-text" style="font-size: 130%"><a href="https://archive.tsearch.eu/search.php?q=david+bowie">Try "David Bowie"</a></p>
              <p class="flow-text" style="font-size: 130%"><a href="https://archive.tsearch.eu/search.php?q=jquery">Try "jQuery"</a></p>
        </div>
    </div>
    </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('#input_text').focus();
    var color = getRandomColor();
    $('#call_back_btn').css("background-color", color);
    $('#text_logo').css("color", color);
    var window_width = $( window ).width();
    var window_height = $( window ).height();
		var document_width = $( document ).width();

    if(window_width < 545) {
      $("#form").css("width", "auto");
      $("#call_back_btn").remove();
      $("#input_text_div").attr("class", "input-field col s12");
      $("#input_text").attr("placeholder", "Press enter to search");
    }
    if(window_width < 411) {
      $("#cat_search_all").remove();
      $("#cat_search_images").remove();
      $("#cat_search_videos").remove();
      $("#cat_search_news").remove();
    }
    if(window_height < 490) {
      $("#form").css("height", "auto");
    }
  });
</script>

  <br />
  <br />
  <br />
  @endsection
