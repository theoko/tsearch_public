@extends('layouts.app')

@section('title', 'Bot')

@section('content')

<div class="container" style="margin-top:100px">
<h1 class="flow-text">Bot</h1><br>
<h6 class="flow-text" style="font-size: 20px">Current Version: 0.1</h6>
<br>
<p class="flow-text" style="font-size: 15px">If you would like to exclude your site from being indexed, send an email to <a href="mailto:exclude@tsearch.eu?subject=Exclude site from tSearch&body=Website address: ...">exclude@tsearch.eu</a></p>
</div>

@endsection
