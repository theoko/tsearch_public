@extends('layouts.app')

@section('title', '404')

@section('content')

<div class="container" style="margin-top:100px">
<h1 class="flow-text">404 / PAGE NOT FOUND</h1><br>
  <h6 class="flow-text" style="font-size: 20px">The page requested was not found. If you believe that this is an error, please report it at <a href="{{ route('report') }}">/report</a><br /><br /><a href="{{ route('home') }}">Return Home</a></h6>
</div>

@endsection
