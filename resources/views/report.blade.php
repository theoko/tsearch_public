@extends('layouts.app')

@section('title', 'Report an issue')

@section('content')
<div class="container" style="margin-top:100px">
<div class="flash-message">
  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <script type="text/javascript">
    	Materialize.toast('{{ Session::get('alert-' . $msg) }}', 10000);
    </script>
    @endif
  @endforeach
</div>
@if(!$issues->isEmpty())
  <ul class="collapsible" data-collapsible="accordion">
    @foreach($issues as $issue)
        <li>
          <div class="collapsible-header"><i class="material-icons">report_problem</i>{{ $issue->name }}</div>
          <div class="collapsible-body"><p>{{ $issue->description }}</p></div>
        </li>
    @endforeach
  </ul>
@endif
<h1 class="flow-text">Report an error/issue</h1>
<form action="{{ route('send_report') }}" method="POST">
<div class="row">
  {!! csrf_field() !!}
  <div class="input-field col s6 m12">
    <input id="name" placeholder="Enter your name..." type="text" style="width:50%" name="name" @if(Auth::user()) value="{{ Auth::user()->name }}" disabled @endif required>
    <label for="name">Your Name *</label>
  </div>
  <div class="input-field col s6 m12">
    <textarea id="description" class="materialize-textarea" name="description" rows="8" cols="40" style="width:75%" placeholder="Enter information regarding your issue..."></textarea>
    <label for="description">Describe the problem *</label>
  </div>
  <div class="input-field col s12"></div>
    <p class="flow-text" style="font-size:90%">If you have any suggestions, include them above. You can view all issues submitted at <a href="/cgi-bin/issues.cgi">issues</a></p>
  <div class="input-field col s2">
  <button id="submit" type="submit" class="waves-effect waves-light btn red lighten-2">SEND REPORT <i class="material-icons right">send</i></button>
  </div>
</div>
</form>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    @if(Auth::user())
      $('#textarea1').focus();
    @else
      $('#name').focus();
    @endif
    $('#textarea1').trigger('autoresize');
  });
</script>
@endsection
