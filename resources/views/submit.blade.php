@extends('layouts.app')

@section('title', 'Submit a site')

@section('content')

<div class="container" style="margin-top:100px">
<div class="flash-message">
@foreach (['danger', 'warning', 'success', 'info'] as $msg)
  @if(Session::has('alert-' . $msg))
  <script type="text/javascript">
  	Materialize.toast('{{ Session::get('alert-' . $msg) }}', 5000);
  </script>
  @endif
@endforeach
</div>
<h1 class="flow-text">Submit a site to tSearch</h1>
<p class="flow-text" id="notice" style="font-size: 120%">Notice: E-mail is needed for verification</p>
  <p class="flow-text" style="font-size: 120%">The site submitted will be crawled in the next 24 hours and added to the index in the next 48 hours.</p>
<form action="/submit" method="POST">
<div class="row">
	{!! csrf_field() !!}
<div class="input-field col s6 m12">
	<input id="email" placeholder="Enter your email..." type="email" class="validate" style="width:50%" name="email" required>
	<label for="email">Email *</label>
</div>
<div class="input-field col s6 m12">
	<input id="site_url" placeholder="Site URL..." type="text" style="width:50%" name="site_url" required>
	<label for="site_url">Site URL *</label>
</div>
<div class="input-field col s3">
	<select id="website_type" name="website_type">
		<option selected disabled>Select a category</option>
		<option>Blog - Personal Website</option>
		<option>Business Website</option>
		<option>eCommerce</option>
		<option>Directory Website</option>
		<option>NonProfit Organization Website</option>
		<option>Online Community (Forum)</option>
		<option>Photo Sharing Website</option>
    <option>Social Networking Website</option>
		<option>Other Website</option>
	</select>
	<label>Website Type</label>
</div>
<div class="input-field col s12"></div>
<div class="input-field col s2">
	<button id="submit" type="submit" class="waves-effect waves-light btn orange lighten-2">SUBMIT <i class="material-icons right">send</i></button>
</div>
</div>
</form>
</div>

<script type="text/javascript">
$(document).ready(function() {
 $('select').material_select();
 $('#email').focus();
});
</script>

@endsection
