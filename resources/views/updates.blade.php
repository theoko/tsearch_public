@extends('layouts.app')

@section('title', 'Updates')

@section('content')

<div class="container" style="margin-top:100px">
<h1 class="flow-text">@foreach($update as $data) {{ $data->title }} ({{ $data->date }}) @endforeach</h1><br>
<h6 class="flow-text" style="font-size: 20px">@foreach($update as $data) {{ $data->description }} @endforeach</h6>
<br />
  <a href="{{ url('/about') }}">Back</a>
</div>

@endsection
