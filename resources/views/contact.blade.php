@extends('layouts.app')

@section('title', 'Contact')

@section('content')

<div class="container" style="margin-top:100px">
<h1 class="flow-text">Contact</h1><br>
<h6 class="flow-text" style="font-size: 20px">Feel free to contact me at <a href="mailto:theodore@tsearch.eu">theodore@tsearch.eu</a> or at <a href="mailto:konstantopoulostheodore@gmail.com">konstantopoulostheodore@gmail.com</a></h6>
<br>
<p class="flow-text" style="font-size: 15px">If you would like to exclude your site from being indexed, send an email to <a href="mailto:exclude@tsearch.eu?subject=Exclude site from tSearch&body=Website address: ...">exclude@tsearch.eu</a><br>Learn more about tSearch Bot at <a href="/bot">/bot</a></p>
</div>

@endsection
