@extends('layouts.app')

@section('title', 'Chemistry Generator')

@section('content')
  <div class="container" style="margin-top: 5%;">
    <div class="row">
      <div class="col s12">
        <div class="row">
            <div class="input-field col s6">
              <input placeholder="Carbon" id="carbon" type="text" class="validate">
              <label for="carbon">Carbon</label>
            </div>
            <div class="input-field col s6">
              <input placeholder="Nitrogen" id="nitrogen" type="text" class="validate">
              <label for="nitrogen">Nitrogen</label>
            </div>
            <div class="input-field col s6">
              <input placeholder="Oxygen" id="oxygen" type="text" class="validate">
              <label for="oxygen">Oxygen</label>
            </div>
            <div class="input-field col s6">
              <input placeholder="Hydrogen" id="hydrogen" type="text" class="validate">
              <label for="hydrogen">Hydrogen</label>
            </div>
            <div class="input-field col s6">
              <input placeholder="Number of multiple bonds" id="mbonds" type="text" class="validate">
              <label for="mbonds">Number of multiple bonds</label>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action" id="submit" style="offset-s6">Send to generator
              <i class="material-icons right">send</i>
            </button>
        </div>
      </div>
    </div>
    <div class="row">
      <div id="result">
      </div>
  </div>
  <script src="/frontend/v0.1/apps/chem/index.js">
@endsection
