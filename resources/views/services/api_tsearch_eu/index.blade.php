@extends('layouts.app')

@section('title', 'API')

@section('content')

<style>
body {
  margin: 0;
  background: #000;
}
video {
    position: fixed;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -100;
    transform: translateX(-50%) translateY(-50%);
  background-size: cover;
  transition: 1s opacity;
}
.stopfade {
   opacity: .5;
}

@media screen and (max-width: 500px) {
  div{width:70%;}
}
</style>

<video id="bgvid" playsinline autoplay muted loop>
    <source src="https://static3.tsearch.eu/api/files/uploads/xEskG/14GP8Ridx2In4U0uWk9y.webm" type="video/webm">
    <source src="https://static3.tsearch.eu/api/files/uploads/eqZX9/bfWc2ymOGrArMCJQZ1Rm.mp4" type="video/mp4">
</video>
<div class="section" style="margin-top: 100px">
    <div class="row">
        <div class="container center-align">
            <h3 class="white-text">Welcome to API!</h3>
            <p class="flow-text white-text">Start building apps today! <button class="waves-effect waves-light btn">Sign in</button> to get started.</p>
        </div>
    </div>
</div>

@endsection
