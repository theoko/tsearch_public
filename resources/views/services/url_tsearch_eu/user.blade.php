@extends('layouts.app')

@section('title', 'My Links')

@section('content')
<style media="screen">
  h2 {
    color: #e57373;
  }
</style>
<div class="row" style="margin-top: 5%">
  <div class="container">
            <div class="col s12 m12">
            <h2 class="header">Links Shortened</h2>
            <div class="card horizontal">
              <div class="card-stacked">
                <div class="card-content">
                  @foreach($urls as $url)
                    <p style="margin-top: 2%;"><a href="https://u.tsearch.eu/{!! $url->title !!}">{{ $url->url }}</a></p>
                  @endforeach
                </div>
                  @if($urls->links())
                    <div class="divider"></div>
                    <div align="center">
                      {{ $urls->appends(Request::except('page'))->links() }}
                    </div>
                  @endif
              </div>
              <br />
            </div>
          </div>
  </div>
</div>
@endsection
