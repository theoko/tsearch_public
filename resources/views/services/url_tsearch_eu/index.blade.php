@extends('layouts.app')

@section('title', 'Shorten a URL')

@section('content')
<div class="flash-message" style="margin-top: 30px">
@foreach (['danger', 'warning', 'success', 'info'] as $msg)
  @if(Session::has('alert-' . $msg))
    <p class="flow-text center-align">{!! Session::get('alert-' . $msg) !!}</p>
    @if(Auth::user())
    <p class="flow-text center-align"><a href="{{ route('url.shortener.view') }}">View all links</a></p>
    @endif
  @endif
@endforeach
</div>
  <div class="row" style="margin-top:300px">
    <div id="header" align="center">
        <h4 id="text_logo">
            Enter a valid URL below
        </h4>
    </div>
    <form action="{{ route('url.shortener.submit') }}" method="post">
      <div class="row">
        {!! csrf_field() !!}
        <div class="input-field col s12 m6" id="input_text_div">
          <input placeholder="Enter a valid URL to shorten..." id="shorten input_text" type="text" class="validate" name="url" autofocus>
          <label for="shorten">URL to shorten</label>
        </div>
        <button type="submit" class="waves-effect waves-light btn orange lighten-2" id="call_back_btn" name="submit" style="float:right;margin-top:20px">Shorten <i class="material-icons right">done</i></button>
      </div>
    </form>
  </div>
@endsection
