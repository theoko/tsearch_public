@extends('layouts.app')

@section('title', 'Images for '.$query)

@section('content')
{{ header("content-type: text/html;charset=utf-8") }}
<style>
    @media only screen and (max-width: 480px){
    .form input[type=text] {
      display:inline-block;

    }
  }
  .center-cols > .col {
  float:none; /* disable the float */
  display: inline-block; /* make blocks inline-block */
  text-align: initial; /* restore text-align to default */
}
</style>
    <div class="container">
      <br><br><br><br><br><br><br><br>
          <div class="row form">
            <form method="get" action="/search/images" class="col s12">
                <div class="row" style="margin-top: -55px">
                  {!! csrf_field() !!}
                  <div class="input-field col s11" style="margin-top: -10px" align="center" id="input_text_div">
                            <input id="input_text" type="text" name="q" value="{{ $query }}">
                            <label for="input_text">Search</label>
                    </div>
                      <button type="submit" class="waves-effect waves-light btn right" id="call_back_btn" name="submit" value="Search" style="float:right;"><i class="fa fa-search"></i></button>
                  </div>
          </form>
        </div>
    </div>
  </div>

  <div class="row container">
  <div class="col s12">
    <ul class="tabs">
      <li class="tab col s12" id="all"><a href="/search/all?q={{ $query }}" target="_blank">WEB</a></li>
      <li class="tab col s12"><a class="active" href="#images" target="_blank">IMAGES</a></li>
      <li class="tab col s12" id="videos"><a href="/search/videos?q={{ $query }}" target="_blank">VIDEOS</a></li>
      <li class="tab col s12" id="news"><a href="/search/news?q={{ $query }}">NEWS</a></li>
    </ul>
  </div>
  <div id="images" class="col s12">
  <br>
  <p class="flow-text" style="color:#ffb74d;font-size:15px">{{ $results_count }} results found in {{ $results_log[2]['time']/1000 }} seconds</p>
  <div class="row center-cols center-align" id="images_results">
      @if($results_count == 0)
        <h4 class="left">Suggestions:<br><br>
      @if(str_slug($query, '_') != $query)<a href="//tsearch.eu/search/images?q={!! str_slug($query, '_') !!}" style="font-size:70%">Try "{{ str_slug($query, '_') }}"</a><br><br>@endif
      @if(str_plural($query) != $query)
        <a href="//tsearch.eu/search/images?q={!! str_plural($query) !!}" style="font-size:70%">Try "{{ str_plural($query) }}"</a><br>
      @else
        <a href="//tsearch.eu/search/images?q={!! str_singular($query) !!}" style="font-size:70%">Try "{{ str_singular($query) }}"</a><br>
      @endif
    </h4>
    @endif
  @foreach($results as $data)
    <div class="col s12 m3">
      <div class="card hoverable">
      <div class="card-image hoverable">
        <img src="{{ $data->link }}" style="max-width: 400px;max-height:400px;" alt="Image not found" onError="this.src='//static3.tsearch.eu/api/files/img/image_not_available.gif';">
      </div>
      <div class="card-content">
         <p>
           <a href="{{ $data->link }}" class="flow-text truncate" style="font-size: 12px;">{{ $data->link }}</a>
        </p>
      </div>
    </div>
    </div>
  @endforeach
  </div>
  <div align="center">
  {{ $results->appends(Request::except('page'))->links() }}
</div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('ul.tabs').tabs();

  $('#all').click(function() {
    window.location.href = 'https://tsearch.eu/search/all?q={!! $query !!}';
  });
  $('#videos').click(function() {
    window.location.href = 'https://tsearch.eu/search/videos?q={!! $query !!}';
  });
  $('#news').click(function() {
    window.location.href = 'https://tsearch.eu/search/news?q={!! $query !!}';
  });

  var window_width = $( window ).width();
  var window_height = $( window ).height();
  var document_width = $( document ).width();

  if(window_width < 1325) {
    $("#call_back_btn").remove();
    $("#input_text_div").attr("class", "input-field col s12");
    $("#input_text").attr("placeholder", "Press enter to search");
  }

  });
</script>

@endsection
