@extends('layouts.app')

@section('title', 'Articles for '.$query)

@section('content')
{{ header("content-type: text/html;charset=utf-8") }}
<style>
    @media only screen and (max-width: 480px){
    .form input[type=text] {
      display:inline-block;

    }
  }
  .center-cols > .col {
  float:none; /* disable the float */
  display: inline-block; /* make blocks inline-block */
  text-align: initial; /* restore text-align to default */
}
</style>
    <div class="container">
      <br><br><br><br><br><br><br><br>
          <div class="row form">
            <form method="get" action="/search/news" class="col s12">
                <div class="row" style="margin-top: -55px">
                  {!! csrf_field() !!}
                  <div class="input-field col s11" style="margin-top: -10px" align="center" id="input_text_div">
                            <input id="input_text" type="text" name="q" value="{{ $query }}">
                            <label for="input_text">Search</label>
                    </div>
                      <button type="submit" class="waves-effect waves-light btn right" id="call_back_btn" name="submit" value="Search" style="float:right;"><i class="fa fa-search"></i></button>
                  </div>
          </form>
        </div>
    </div>
  </div>

  <div class="row container">
  <div class="col s12">
    <ul class="tabs">
      <li class="tab col s12" id="all"><a href="/search/all?q={{ $query }}" target="_blank">WEB</a></li>
      <li class="tab col s12" id="images"><a href="/search/images?q={{ $query }}" target="_blank">IMAGES</a></li>
      <li class="tab col s12" id="videos"><a href="/search/videos?q={{ $query }}" target="_blank">VIDEOS</a></li>
      <li class="tab col s12"><a class="active" href="#news">NEWS</a></li>
    </ul>
  </div>
  <div id="news" class="col s12">
  <br>
  <p class="flow-text" style="color:#ffb74d;font-size:15px">{{ $results_count }} results found in {{ $results_log[2]['time']/1000 }} seconds</p>
  <div class="row center-cols center-align" id="news_results">
    @if($results_count == 0)
      <a href="https://news.tsearch.eu/search_news.php?q={!! $query !!}">No results were found. However, you can still search news for "{{ $query }}"</a><br><br>
        <h4 class="left">Suggestions:<br><br>
      @if(str_slug($query, '_') != $query)<a href="//tsearch.eu/search/news?q={!! str_slug($query, '_') !!}" style="font-size:70%">Try "{{ str_slug($query, '_') }}"</a><br><br>@endif
      @if(str_plural($query) != $query)
        <a href="//tsearch.eu/search/news?q={!! str_plural($query) !!}" style="font-size:70%">Try "{{ str_plural($query) }}"</a><br>
      @else
        <a href="//tsearch.eu/search/news?q={!! str_singular($query) !!}" style="font-size:70%">Try "{{ str_singular($query) }}"</a><br>
      @endif
    </h4>
    @endif
  @if($results_count > 0)
    <div class="row">
      <div class="col s12 m6" style="width:100%">
        <div class="card yellow lighten-4">
          <div class="card-content">
            <p>
  @foreach($results as $data)
  <div class="row">
    <div class="col s12 m6" style="width:100%">
      <div class="card yellow lighten-5">
        <div class="card-content">
  @if(empty(trim($data->title)))
    <p class="left"><a class="truncate" href="{{ $data->link }}">{{ $data->link }}</a></p>
  @else
    <p class="left"><a class="truncate" href="{{ $data->link }}">{{ $data->title }}</a></p>
  @endif
  @if(empty(trim($data->description)))
    <br><p class="left"><br>No description was found.</p><br><br>
  @else
    <br><br><p>{{ str_limit($data->description, 300) }}</p><br>
  @endif
  @if($data->keywords != 'No keywords were found.')
    @if(count(array_filter(explode(',', $data->keywords))) > 5)
      <div id="{{ $data->id }}" style="display: none;">
    @endif
    @foreach(array_filter(explode(',', $data->keywords)) as $keyword)

      <div class="chip">
        <a href="https://tsearch.eu/search/news?q={!! $keyword !!}" style="color: black;">{{ $keyword }}</a>
        <i class="material-icons">close</i>
      </div>

    @endforeach
    @if(count(array_filter(explode(',', $data->keywords))) > 5)
      </div>
    @endif
    @if(count(array_filter(explode(',', $data->keywords))) > 5)
      <a href="#show-{{ $data->id }}" id="{{ $data->id }}" class="show_keywords">Show keywords ({{ count(array_filter(explode(',', $data->keywords))) }})</a>
    @endif
  @endif
    </div>
  </div>
</div>
</div>
  @endforeach
  <div align="center">
  {{ $results->appends(Request::except('page'))->links() }}
</div>
</p>
</div>
</div>
</div>
</div>
@endif
  </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('ul.tabs').tabs();

  $('#all').click(function() {
    window.location.href = 'https://tsearch.eu/search/all?q={!! $query !!}';
  });
  $('#images').click(function() {
    window.location.href = 'https://tsearch.eu/search/images?q={!! $query !!}';
  });
  $('#videos').click(function() {
    window.location.href = 'https://tsearch.eu/search/videos?q={!! $query !!}';
  });

    $('.show_keywords').click(function() {
      var show = $(this).attr("id");
      $("#"+show).show();
    });

    var window_width = $( window ).width();
    var window_height = $( window ).height();
		var document_width = $( document ).width();

    if(window_width < 1325) {
      $("#call_back_btn").remove();
      $("#input_text_div").attr("class", "input-field col s12");
      $("#input_text").attr("placeholder", "Press enter to search");
    }

  });
</script>

@endsection
