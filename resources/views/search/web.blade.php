@extends('layouts.app')

@section('title', 'Web search for '.$data['query'])

@section('content')
<style>
    @media only screen and (max-width: 480px){
    .form input[type=text] {
      display:inline-block;

    }
  }
  .center-cols > .col {
  float:none; /* disable the float */
  display: inline-block; /* make blocks inline-block */
  text-align: initial; /* restore text-align to default */
}

i.icon-green {
    color: green;
}

    #pagination {
        align: center;
    }
</style>
    <div class="container">
      <br><br><br><br><br><br><br><br>
          <div class="row form">
            <form method="get" action="{{ route('search') }}" class="col s12">
                <div class="row" style="margin-top: -55px">
                  <div class="input-field col s11" style="margin-top: -10px" align="center" id="input_text_div">
                            <input id="input_text" type="text" name="q" value="{{ $data['query'] }}">
                            <label for="input_text">Search</label>
                    </div>
                      <button type="submit" class="waves-effect waves-light btn right" id="call_back_btn" name="submit" value="Search" style="float:right;"><i class="fa fa-search"></i></button>
                  </div>
          </form>
        </div>
    </div>

  <div class="row container">
  <div class="col s12">
    <ul class="tabs">
      <li class="tab col s12"><a class="active" href="{{ route('search') }}?q={{ $data['query'] }}">WEB</a></li>
      <li class="tab col s12" id="images"><a href="{{ route('search.images') }}?q={{ $data['query'] }}">IMAGES</a></li>
      <li class="tab col s12" id="videos"><a href="{{ route('search.videos') }}?q={{ $data['query'] }}">VIDEOS</a></li>
      <li class="tab col s12" id="news"><a href="{{ route('search.news') }}?q={{ $data['query'] }}">NEWS</a></li>
    </ul>
  </div>
  <div id="all" class="col s12">
  <br>
  <p class="flow-text" style="color:#ffb74d;font-size:15px">{{ $data['hits'] }} results found in {{ $data['took'] }} seconds</p>
  <div class="center-cols">
    @if(!isset($data['results']))
      <div class="left">{!! $data['noResultsMessage'] !!}</div>
    @endif
</div>
  @if($data['hits'] > 0)
  <div class="row">
    <div class="col s12 m6" style="width:100%">
          @foreach($data['results'] as $r)

              <div class="card white lighten-5">
                <div class="card-content">
                  <p class="truncate"><a href="{{ $r['url'] }}">{{ $r['title'] }}</a></p>
                  <p class="green-text truncate">{{ $r['url'] }}</p>
                  <p class="grey-text lighten-1">{{ $r['timestamp'] }} <span class="grey-text lighten-2"> - {{ $r['description'] }}</span></p>
                    @if(isset($r['htmlTable']))
                        {!! $r['htmlTable'] !!}
                    @endif
                  <a class="btn-flat" href="{{ route('cache.web', ['blobID' => $r['cacheID']]) }}">Cached</a>
            </div>
          </div>

          @endforeach

        <div id="pagination">{!! $data['pagination'] !!}</div>
  </div>
  </div>
@endif
</div>

<script>
  $(document).ready(function(){
    $('ul.tabs').tabs();

  $('#images').click(function() {
    window.location.href = '{{ route('search.images') }}?q={!! $data['query'] !!}';
  });
  $('#videos').click(function() {
    window.location.href = '{{ route('search.videos') }}?q={!! $data['query'] !!}';
  });
  $('#news').click(function() {
    window.location.href = '{{ route('search.news') }}?q={!! $data['query'] !!}';
  });

  var window_width = $( window ).width();
  var window_height = $( window ).height();
  var document_width = $( document ).width();

  if(window_width < 1325) {
    $("#call_back_btn").remove();
    $("#input_text_div").attr("class", "input-field col s12");
    $("#input_text").attr("placeholder", "Press enter to search");
  }

  });
</script>

@endsection
