@extends('layouts.app')

@section('title', 'Videos for '.$query)

@section('content')
{{ header("content-type: text/html;charset=utf-8") }}
<style>
    @media only screen and (max-width: 480px){
    .form input[type=text] {
      display:inline-block;

    }
  }
  .center-cols > .col {
  float:none; /* disable the float */
  display: inline-block; /* make blocks inline-block */
  text-align: initial; /* restore text-align to default */
}
</style>
    <div class="container">
      <br><br><br><br><br><br><br><br>
          <div class="row" class="form">
            <form method="get" action="/search/videos" class="col s12">
                <div class="row" style="margin-top: -55px">
                  {!! csrf_field() !!}
                  <div class="input-field col s11" style="margin-top: -10px" align="center" id="input_text_div">
                            <input id="input_text" type="text" name="q" value="{{ $query }}">
                            <label for="input_text">Search</label>
                    </div>
                      <button type="submit" class="waves-effect waves-light btn right" id="call_back_btn" name="submit" value="Search" style="float:right;"><i class="fa fa-search"></i></button>
                  </div>
          </form>
        </div>
    </div>
  </div>

  <div class="row container">
  <div class="col s12">
    <ul class="tabs">
      <li class="tab col s12" id="all"><a href="/search/all?q={{ $query }}">WEB</a></li>
      <li class="tab col s12" id="images"><a href="/search/images?q={{ $query }}">IMAGES</a></li>
      <li class="tab col s12"><a class="active" href="#videos">VIDEOS</a></li>
      <li class="tab col s12" id="news"><a href="/search/news?q={{ $query }}">NEWS</a></li>
    </ul>
  </div>
  <div id="videos" class="col s12">
  <br>
  <p class="flow-text" style="color:#ffb74d;font-size:15px">{{ $results_count }} results found in {{ $results_log[2]['time']/1000 }} seconds</p>
  <div class="row center-cols center-align" id="videos_results">
    <div class="carousel">
  @foreach($results as $data)
    <a class="carousel-item" href="{{ $data->link }}"><img src="{{ $data->thumb }}"></a>
  @endforeach
  </div>
  @foreach($results as $data)
    <div class="row">
    <div class="col s12 m6" style="width:100%">
      <div class="card yellow lighten-5">
        <div class="card-content">
  @if(empty(trim($data->title)))
    <p><img class="left materialboxed" width="150" src="{{ $data->thumb }}" style="width:150px;height:100px" alt="No title found."><a href="{{ $data->link }}">{{ $data->website }} | No title found.</a></p>
  @else
    <p><img class="left materialboxed" width="150" src="{{ $data->thumb }}" style="width:150px;height:100px" alt="{{ $data->title }}"><a href="{{ $data->link }}">{{ $data->website }} | {{ $data->title }}</a></p>
  @endif
  @if(empty(trim($data->duration)))
    <br><p><br>No duration was found.<br>Date Added: {{ $data->date }}</p><br><br>
  @else
    <br><p>Duration: {{ $data->duration }}<br>Date Added: {{ $data->date }}</p><br>
  @endif
    </div>
  </div>
</div>
</div>
  @endforeach
  </div>
  <div align="center">
  {{ $results->appends(Request::except('page'))->links() }}
</div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('ul.tabs').tabs();
    @if($results_count > 0)
    $('.carousel').carousel();
    $('.materialboxed').materialbox();
    @endif

  $('#all').click(function() {
    window.location.href = 'https://tsearch.eu/search/all?q={!! $query !!}';
  });
  $('#images').click(function() {
    window.location.href = 'https://tsearch.eu/search/images?q={!! $query !!}';
  });
  $('#news').click(function() {
    window.location.href = 'https://tsearch.eu/search/news?q={!! $query !!}';
  });

  var window_width = $( window ).width();
  var window_height = $( window ).height();
  var document_width = $( document ).width();

  if(window_width < 1325) {
    $("#call_back_btn").remove();
    $("#input_text_div").attr("class", "input-field col s12");
    $("#input_text").attr("placeholder", "Press enter to search");
  }

  });
</script>

@endsection
