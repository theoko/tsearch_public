@extends('layouts.app')

@section('title', 'Search Insights')

@section('content')
<div class="row" style="width:100%;">
  <div class="container" style="margin-top:100px">
    <div class="container">
      <h3 class="flow-text left" style="margin-left: -20px">INDEX STATISTICS</h3>
      <h3 class="flow-text right" style="margin-right: -20px">USER STATISTICS</h3>
    </div>
  </div>
  <div class="container" style="margin-top:200px">
    <div id="index-donut" class="left"></div>
    <div id="queries-donut" class="right"></div>
  </div>
  <div class="container" style="margin-top: 600px">
    <h3 class="flow-text" style="text-align:center">TOP 10 SEARCHES</h3>
    <div id="top-searches-donut"></div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  Morris.Donut({
  element: 'index-donut',
  data: [
    {label: "WEB PAGES", value: {{ $main }}},
    {label: "IMAGES", value: {{ $images }}},
    {label: "VIDEOS", value: {{ $videos }}},
    {label: "NEWS", value: {{ $news }}},
    {label: "TOTAL", value: {{ $total }}}
  ],
  resize: true
  });

  Morris.Donut({
  element: 'top-searches-donut',
  data: [
    @foreach($top_searches as $top_search)
      {label: "{!! $top_search->query !!}", value: {{ $top_search->occurrences }}},
    @endforeach
  ],
  resize: true
  });

  Morris.Donut({
  element: 'queries-donut',
  data: [
    {label: "WEB SEARCH", value: {{ $queries_main }}},
    {label: "IMAGES SEARCH", value: {{ $queries_images }}},
    {label: "VIDEOS SEARCH", value: {{ $queries_videos }}},
    {label: "NEWS SEARCH", value: {{ $queries_news }}},
    {label: "ADVANCED SEARCH", value: {{ $queries_advanced }}},
    {label: "TOTAL SEARCHES", value: {{ $total_queries }}}
  ],
  resize: true
  });
});
</script>
@endsection
