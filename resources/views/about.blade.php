@extends('layouts.app')

@section('title', 'About')

@section('content')

<div class="container" style="margin-top:100px">
<h1 class="flow-text">About</h1><br>
<h6 class="flow-text" style="font-size: 20px">tSearch is a PHP-based project, a small search-engine powered by <a href="https://archive.tsearch.eu/search.php?q=Elasticsearch">Elasticsearch</a>.</h6>
<br>
<p class="flow-text" style="font-size: 15px">If you would like to exclude your site from being indexed, send an email to <a href="mailto:exclude@tsearch.eu?subject=Exclude site from tSearch&body=Website address: ...">exclude@tsearch.eu</a><br>Learn more about tSearch Bot at <a href="/bot">/bot</a></p>
</div>

@endsection
