@extends('layouts.app')

@section('title', 'Search')

@section('content')
    <div class="col s12 m6">
    	  <div id="header" align="left"><h1 class="flow-text">tSearch</h1>
          <div class="row">
            <form method="get" action="/search/" class="col s12">
                <div class="row">
                  <div class="input-field col s10" style="margin-top: -10px">
                            <input id="input_text" type="text" placeholder="Type a keyword">
                            <label for="input_text">Search</label>
                    </div>
                      <button type="submit" class="waves-effect waves-light btn" id="call_back_btn" name="submit" value="Search" style="float:right;"><i class="fa fa-search"></i></button>
                  </div>
          </form>
        </div>
    </div>
  </div>

  <br />
  <br />
  <br />
  @endsection
