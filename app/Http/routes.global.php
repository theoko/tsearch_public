<?php

// Global Routes
Route::auth();

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::post('/git/app/deploy', function () {
  dispatch(new App\Jobs\GitDeploy);
});

// User

//Route::get('/profile', [
//  'middleware' => 'auth',
//  'uses' => 'User\UserController@profile',
//]);
//
//Route::post('/profile', [
//  'middleware' => 'auth',
//  'uses' => 'User\UserController@update_avatar',
//]);
