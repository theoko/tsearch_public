<?php

Route::group(['domain' => 'api.tsearch.eu'], function() {

    Route::get('/', [
        'uses' => 'Api\ApiController@index',
        'as' => 'api.home',
    ]);

    Route::get('/action/{url}/{token}/{option}', [
      'uses' => 'Api\ApiController@action',
    ]);

    Route::get('generateapitoken', [
          'middleware' => 'auth',
          'uses' => 'Api\ApiController@generateapitoken',
          'as' => 'api.generateapitoken'
    ]);

//    CRAWLER API
    Route::get('crawler', [
        'middleware' => 'auth',
        'uses' => 'Api\CrawlerController@index',
    ]);

    // Apps
    Route::group(['prefix' => 'apps/chem'], function () {
        Route::get('/', [
          'uses' => 'Api\Apps\Chem\HomeController@index',
          'as' => 'api.app.chem.home',
        ]);

        Route::group(['prefix' => 'api'], function() {
          Route::post('result', [
            'uses' => 'Api\Apps\Chem\HomeController@result',
            'as' => 'api.app.chem.api.result',
          ]);
        });
    });
});
