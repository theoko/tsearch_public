<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use Response;

class ApiController extends Controller
{
    public function index() {
        return view('services.api_tsearch_eu.index');
    }
    
    public function action(Response $response, Request $request, $url, $token, $option) {
            $value = $request->input('v');
            $check_value = trim($value);
            $check_token = trim($token);
            $check_option = trim($option);
            if (empty(trim($check_value)) || empty(trim($check_option)) || empty(trim($check_token))) {
              die();
            }
            switch ($url) {

              case 'passwd':
                  switch ($option) {
                    case 'reset':
                          if (strlen($value) >= 8) {
                            $reset = DB::select('select * from users where api_token = :api_token', ['api_token' => $token]);
                            if ($reset) {
                              $affected = DB::update('update users set password = :new_password where api_token = :api_token', ['new_password' => bcrypt($value),'api_token' => $token]);
                              $message = 'success';
                            } else {
                              return response('error : invalid token', 401);
                            }
                          } else {
                            $message = 'error : passwords should be at least 8 characters long';
                          }
                      break;

                    default:
                      $message = 'error : invalid action';
                    break;
                  }
              break;

              case 'tsearch.eu':
                switch ($option) {
                  case 'retrieve':
                    $auth = DB::select('select * from users where api_token = :api_token', ['api_token' => $token]);
                    if(!$auth) {
                      return response('error : invalid token', 401);
                    } else {
                        $search = DB::select('select id, title, keywords, description, weight, date from main where id = :id', ['id' => $value]);
                        if ($search) {
                          $message = array();
                          foreach($search as $data) {
                            array_push($message, $data->id);
                            array_push($message, $data->title);
                            array_push($message, $data->keywords);
                            array_push($message, $data->description);
                            array_push($message, $data->weight);
                            array_push($message, $data->date);
                          }
                          return json_encode(array('data' => $message));
                        } else {
                          $message = 'error : not found';
                        }
                    }
                  break;

                  case 'retrieve_image':
                      $auth = DB::select('select * from users where api_token = :api_token', ['api_token' => $token]);
                      if(!$auth) {
                        return response('error : invalid token', 401);
                      } else {
                          $image = DB::select('select id, link from images where id = :id', ['id' => $value]);
                          if ($image) {
                            foreach($image as $data) {
                                $filepath = file_get_contents($data->link);
                                $filepath = base64_encode($filepath);
                                    if(str_contains($data->link, '.gif')) {
                                      $image_type = 'image/gif';
                                    } else if(str_contains($data->link, '.jpg') || str_contains($data->link, '.jpeg')) {
                                      $image_type = 'image/jpg';
                                    } else if(str_contains($data->link, '.png')) {
                                      $image_type = 'image/png';
                                    } else {
                                      $message = 'error : image could not be displayed';
                                      return $message;
                                    }
                                $file = 'data:'.$image_type.';base64,'.$filepath;
                                $file = '<img src="'.$file.'">';
                                return $file;
                            }

                          } else {
                            $message = 'error : not found';
                          }
                      }
                  break;

                  case 'retrieve_image_url':
                      $auth = DB::select('select * from users where api_token = :api_token', ['api_token' => $token]);
                      if(!$auth) {
                        return response('error : invalid token', 401);
                      } else {
                          $image = DB::select('select id, link from images where link = :link', ['link' => $value]);
                          if ($image) {
                            foreach($image as $data) {
                                $filepath = file_get_contents($data->link);
                                $filepath = base64_encode($filepath);
                                    if(str_contains($data->link, '.gif')) {
                                      $image_type = 'image/gif';
                                    } else if(str_contains($data->link, '.jpg') || str_contains($data->link, '.jpeg')) {
                                      $image_type = 'image/jpg';
                                    } else if(str_contains($data->link, '.png')) {
                                      $image_type = 'image/png';
                                    } else {
                                      $message = 'error : image could not be displayed';
                                      return $message;
                                    }
                                $file = 'data:'.$image_type.';base64,'.$filepath;
                                $file = '<img src="'.$file.'">';
                                return $file;
                            }

                          } else {
                            $message = 'error : not found';
                          }
                      }
                  break;

                  case 'search':
                    $auth = DB::select('select * from users where api_token = :api_token', ['api_token' => $token]);
                    if(!$auth) {
                      return response('error : invalid token', 401);
                    } else {
                        $results = DB::table('main')->select('*')->where('keywords', 'LIKE', '%'.$value.'%');
                        $results_count = $results->count();
                        $results = $results->paginate(15);
                        if ($results) {
                          $message = array();
                          foreach($results as $data) {
                            array_push($message, $data->id);
                            array_push($message, strip_tags($data->title));
                            array_push($message, strip_tags($data->keywords));
                            array_push($message, strip_tags($data->description));
                            array_push($message, $data->weight);
                            array_push($message, $data->date);
                          }
                          return json_encode(array($results_count => $message));
                        } else {
                          $message = 'no results';
                        }
                    }
                  break;

                  default:
                    $message = 'error : invalid action';
                  break;
                }
              break;

              default:
                $message = "error : service does not exist";
              break;
            }
            return $message;
  }
  
  public function generateapitoken() {
    $api_token = str_random(64);
    $api_token = sha1($api_token);
    $email = Auth::user()->email;

    $insert_token = DB::update('update users set api_token = :new_api_token where email = :email', [
            'new_api_token' => $api_token,
            'email' => $email
    ]);

    return redirect()->route('api.home');
  }
}
