<?php

namespace App\Http\Controllers\Api\Apps\Chem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller {
  public function index() {
    return view('services.api_tsearch_eu.apps.chem.index');
  }

  public function result() {
    return 'this is a test';
  }
}
