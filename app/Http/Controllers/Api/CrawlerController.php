<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Goutte\Client;
use Response;

class CrawlerController extends Controller
{
    public function index(Request $request) {
      $url = $request->input('url');
      $a = $request->input('a');

        if(empty(trim($url))) {
          abort(404);
        }

      $client = new Client();

      $crawler = $client->request('GET', $url);

      $status_code = $client->getResponse()->getStatus();

      if($status_code == 403) {
          return 'the resource returned 403';
      }
      
      $data = [];

      switch ($a) {
        case 'title':
            $data[] = $crawler->filter('title')->each(function ($node) {
                return $node->text();
            });
        break;

        case 'images':
            $data[] = $crawler->filter('img')->each(function ($node) {
              return $node->attr('src');
            });
        break;

        case 'description':
            $data[] = $crawler->filter('meta')->each(function ($node) {
              if(stristr($node->attr('name'), 'description') || stristr($node->attr('property'), 'description')) {
                return $node->attr('content');
              }
            });
        break;

        case 'favicon':
            $data[] = $crawler->filter('link')->each(function ($node) {
            if($node->attr('rel') == 'icon')
              return $node->attr('href');
            });
        break;

        case 'logo':
            $data[] = $crawler->filter('meta')->each(function ($node) {
            if($node->attr('itemprop') == 'image') return $node->attr('content');
            });
        break;

        case 'image:title':
            $data[] = $crawler->filter('img')->each(function ($node) {
              return $node->attr('alt')."<br />";
            });
        break;

        default:
            return abort(400);
        break;
      }
      
      return Response::json($data);

    }
}
