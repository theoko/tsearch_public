<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ReportIssue;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Auth;

class ReportController extends Controller
{
    public function index(Request $request) {
      $issues = ReportIssue::orderBy('id', 'desc')
                          ->take(5)
                          ->get();

      return view('report', ['issues' => $issues]);
    }

    public function submit(Request $request) {
      $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'description' => 'required',
      ]);

      if($validator->fails()) {
        if($validator->messages()->has('name') && $validator->messages()->has('description')) {
          $message = $validator->messages()->first('name');
        } else if ($validator->messages()->has('name')) {
          $message = $validator->messages()->first('name');
        } else if ($validator->messages()->has('description')) {
          $message = $validator->messages()->first('description');
        }
        $request->session()->flash('alert-danger', $message);
        return redirect()->route('report');
      }

      if(Auth::user()) $name = Auth::user()->name; else $name = $request->input('name'); 
      $description = $request->input('description');
      $issue = new ReportIssue;
      $issue->name = $name;
      $issue->description = $description;

      $issue->save();
      $request->session()->flash('alert-success', 'Report submitted. Thank you!');
      return redirect()->route('report');

    }
}
