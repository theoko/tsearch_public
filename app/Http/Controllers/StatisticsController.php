<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class StatisticsController extends Controller
{
    public function index(Request $request) {
      $total = 0;
      $total_queries = 0;

      $count_main_index = DB::table('main')->select('*');
      $count_main_index = $count_main_index->count();

      $total = $total + $count_main_index;

      $count_news_index = DB::table('news')->select('*');
      $count_news_index = $count_news_index->count();

      $total = $total + $count_news_index;

      $count_images_index = DB::table('images')->select('*');
      $count_images_index = $count_images_index->count();

      $total = $total + $count_images_index;

      $count_videos_index = DB::table('videos')->select('*');
      $count_videos_index = $count_videos_index->count();

      $total = $total + $count_videos_index;

      $count_queries = DB::table('queries')->select('*');
      $count_queries = $count_queries->count();

      $total_queries = $count_queries;

      $count_main_queries = DB::table('queries')->select('query')->where('searched_in', '=', 'all');
      $count_main_queries = $count_main_queries->count();

      $count_images_queries = DB::table('queries')->select('query')->where('searched_in', '=', 'images');
      $count_images_queries = $count_images_queries->count();

      $count_news_queries = DB::table('queries')->select('query')->where('searched_in', '=', 'news');
      $count_news_queries = $count_news_queries->count();

      $count_videos_queries = DB::table('queries')->select('query')->where('searched_in', '=', 'videos');
      $count_videos_queries = $count_videos_queries->count();

      $count_advanced_queries = DB::table('queries')->select('query')->where('searched_in', '=', 'advanced');
      $count_advanced_queries = $count_advanced_queries->count();

      // GET TOP 10

      $top_searches = DB::table('queries')
      ->selectRaw('query, COUNT(*) as occurrences')
      ->where('results', '>', '0')
      ->groupBy('query')
      ->orderBy('occurrences', 'desc');

      $top_searches = $top_searches->take(10)->get();

      return view('statistics', ['top_searches' => $top_searches, 'total' => $total, 'total_queries' => $total_queries, 'main' => $count_main_index, 'news' => $count_news_index, 'images' => $count_images_index, 'videos' => $count_videos_index, 'queries_main' => $count_main_queries, 'queries_images' => $count_images_queries, 'queries_videos' => $count_videos_queries, 'queries_news' => $count_news_queries, 'queries_advanced' => $count_advanced_queries]);
    }
}
