<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Image;

class UserController extends Controller
{
   public function profile() {
     return view('profile', array('user' => Auth::user()));
   }

   public function update_avatar(Request $request) {
     if ($request->hasFile('avatar')) {
       $avatar = $request->file('avatar');
       $filename = time() . '.' . $avatar->getClientOriginalExtension();
//       Should write to the static server
       Image::make($request->file('avatar'))->resize(300, 300)->save(public_path('/uploads/avatars/'.$filename));

       $user = Auth::user();
       $user->avatar = $filename;
       $user->save();
     }

     return view('profile', array('user' => Auth::user()));
   }
}
