<?php

namespace App\Http\Controllers\Url;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UrlShortener;

use DB;
use Auth;
use Validator;

class UrlController extends Controller
{
    public function index() {
      return view('services.url_tsearch_eu.index');
    }

    public function viewUrls(Request $request) {
        $urls = UrlShortener::where('user', '=', Auth::user()->email)->orderBy('id', 'desc');
        $urls = $urls->paginate(10);
        if($urls) {
          return view('services.url_tsearch_eu.user', ['urls' => $urls]);
        } else {
          $request->session()->flash('alert-danger', 'You have not shortened any urls yet!');
          return redirect()->route('url.shortener.home');
        }
    }

    public function submit(Request $request) {
      $validator = Validator::make($request->all(), [
        'url' => 'required|max:255|url',
      ]);
      if($validator->fails()) {
        $request->session()->flash('alert-success', $validator->messages()->first('url'));
        return redirect()->route('url.shortener.home');
      }

      $url = $request->input('url');
      $random = str_random(10);
      if(Auth::user()) {
          $user = Auth::user()->email;
      } else {
          $user = 'none';
      }

      $shortener = new UrlShortener;
      $shortener->url = $url;
      $shortener->title = $random;
      $shortener->user = $user;

      $shortener->save();
      $request->session()->flash('alert-success', 'URL shortened to <a href="https://u.tsearch.eu/'.$random.'">u.tsearch.eu/'.$random.'</a>');
      return redirect()->route('url.shortener.home');

    }
}
