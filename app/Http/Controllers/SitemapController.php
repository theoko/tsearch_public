<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Sitemap;

use Route;

class SitemapController extends Controller
{
  public function index()
  {
      // Get a general sitemap.
      Sitemap::addSitemap('/sitemap/general');

      // Return the sitemap to the client.
      return Sitemap::index();
  }

  public function general() {
    $app_url = url('/');
    $routes = Route::getRoutes();
    $paths = array();

    foreach($routes as $value) {
      $value = $value->getPath();
      if(!stristr($value, '{') && !stristr($value, 'sitemap')) {
        if(substr($value, -1) == '/') {
          $value = $app_url.$value;
        } else {
          $value = $app_url.'/'.$value;
        }
        if(!in_array($value, $paths)) {
          array_push($paths, $value);
        }
      }
    }

    foreach($paths as $path) {
      Sitemap::addTag($path);
    }

    return Sitemap::render();
  }
}
