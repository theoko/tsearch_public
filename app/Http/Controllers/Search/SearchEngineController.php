<?php

namespace App\Http\Controllers\Search;

use App\SearchEngine\Filter;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SearchEngine\Search;
use App\SearchEngine\Cache;

class SearchEngineController extends Controller
{
    public function getResults(Request $request) {
        $query = $request->input('q');

        $search = new Search($query);
        if(!$search->validateQuery()) return redirect()->route('home');
        if(!$search->validatePage($request)) return redirect()->route('home');
        $data = $search->getResults();

        return view('search.web', ['data' => $data]);
    }

    public function getWebCache($blobID) {
        $cache = new Cache($blobID);
        if(!$cache->validateID()) return redirect()->route('home');
        $data = $cache->getCachedPage();
        if($data) return view('cache.web', ['data' => $data]); else return redirect()->route('home');
    }

//    REFERER API
/*    public function getReferer(Request $request) {
*        $query = $request->input('q');
*
*       $search = new Search($query);
*        $r = new Filter();
*        if(!$search->validateQuery()) return redirect()->route('home');
*        $data = $r->getRowPages($query);
*
*        return $data['htmlTable'];
*    }
*/

}
