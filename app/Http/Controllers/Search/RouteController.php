<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
    public function index(Request $request) {
        return view('index');
    }

    public function about(Request $request) {
        return view('about');
    }

    public function bot(Request $request) {
        return view('bot');
    }
}
