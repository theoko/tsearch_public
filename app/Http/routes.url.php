<?php

Route::group(['domain' => 'url.tsearch.eu'], function () {
     Route::get('/', [
    'uses' => 'Url\UrlController@index',
     'as' => 'url.shortener.home',
    ]);

    Route::post('/shorten', [
      'uses' => 'Url\UrlController@submit',
      'as' => 'url.shortener.submit',
    ]);    
    
    Route::get('/view', [
      'middleware' => 'auth',
      'uses' => 'Url\UrlController@viewUrls',
      'as' => 'url.shortener.view',
    ]);  
});

