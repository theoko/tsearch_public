<?php

Route::group(['domain' => 'tsearch.eu'], function() {

Route::get('/', [
    'uses' => 'Search\RouteController@index',
    'as' => 'home',
]);

Route::get('/search/all', [
    'uses' => 'Search\SearchEngineController@getResults',
]);

Route::get('/search/web', [
  'uses' => 'Search\SearchEngineController@getResults',
  'as' => 'search',
]);

Route::get('/cache/{blobID}', [
    'uses' => 'Search\SearchEngineController@getWebCache',
    'as' => 'cache.web',
]);

// Route::get('/search/images', [
//   'uses' => 'SearchController@search_images',
//   'as' => 'search.images',
// ]);
Route::get('/search/images', function() {
   return redirect()->route('home');
})->name('search.images');

// Route::get('/search/videos', [
//   'uses' => 'SearchController@search_videos',
//   'as' => 'search.videos',
// ]);
Route::get('/search/videos', function() {
   return redirect()->route('home');
})->name('search.videos');


Route::get('/search/news', [
  'uses' => 'Search\SearchEngineController@getResults',
  'as' => 'search.news',
]);

Route::get('/search/advanced', [
  'uses' => 'SearchController@advanced',
  'as' => 'advanced',
]);

Route::get('/contact', [
  'uses' => 'SearchController@contact',
  'as' => 'contact',
]);

Route::get('/bot', [
  'uses' => 'Search\RouteController@bot',
  'as' => 'botinfo',
]);

Route::get('/about', [
  'uses' => 'Search\RouteController@about',
  'as' => 'about',
]);

Route::get('/submit', [
  'uses' => 'SearchController@submit',
  'as' => 'submit',
]);

Route::post('/submit', [
  'uses' => 'SearchController@submitted',
]);

Route::get('/verify/site/{token}', [
  'uses' => 'SearchController@verify',
]);

// SEARCH SETTINGS

Route::get('/search/settings', [
  'uses' => 'SearchController@settings',
   'as' => 'settings',
]);

// STATISTICS

Route::get('/statistics', [
  'uses' => 'StatisticsController@index',
  'as' => 'statistics',
]);

// SEARCH SUGGESTIONS

Route::get('/search/suggest/{query}', [
  'uses' => 'SearchController@get_suggestions',
]);

// UPDATES

Route::get('/latest/updates/{update}', [
  'uses' => 'SearchController@updates',
]);

// REPORTS

Route::get('/report', [
  'uses' => 'Report\ReportController@index',
  'as' => 'report',
]);

Route::post('/report', [
  'uses' => 'Report\ReportController@submit',
  'as' => 'send_report',
]);

// SITEMAPS

Route::get('/sitemap', 'SitemapController@index');
Route::get('/sitemap/general', 'SitemapController@general');
Route::get('/sitemap.xml', 'SitemapController@general');

});

Route::group(['domain' => 'www.tsearch.eu'], function () {
   Route::any( '/', function() {
       return Redirect::route('home');
   });
});
