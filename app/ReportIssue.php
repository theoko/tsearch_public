<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportIssue extends Model
{
    protected $table = 'issues';
    public $timestamps = true;
}
