<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\ProcessBuilder;

class GitDeploy extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $process = new Process('cd /var/sites/tsearch && git pull');
        $process->run();

      $process1 = new Process('cd /var/sites/tsearch && git submodule sync');
      $process1->run();

      $process2 = new Process('cd /var/sites/tsearch && git submodule update');
      $process2->run();

        $process3 = new Process('cd /var/sites/tsearch && git submodule status');
        $process3->run();

        if (!$process->isSuccessful()) {
            Log::critical('Git pull failed! '.$process->getOutput());
            throw new ProcessFailedException($process);
        }

      if (!$process1->isSuccessful()) {
          Log::critical('Git pull failed! '.$process1->getOutput());
          throw new ProcessFailedException($process1);
      }
      if (!$process2->isSuccessful()) {
          Log::critical('Git pull failed! '.$process2->getOutput());
          throw new ProcessFailedException($process2);
      }
      if (!$process3->isSuccessful()) {
          Log::critical('Git pull failed! '.$process3->getOutput());
          throw new ProcessFailedException($process3);
      }
      if($process1->isSuccessful() && $process2->isSuccessful() && $process2->isSuccessful() && $process->isSuccessful()) {
        Log::info('Git pull succeded. Application up-to-date!');
      }
    }
}
