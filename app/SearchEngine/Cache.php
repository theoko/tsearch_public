<?php
/**
 * Created by PhpStorm.
 * User: theoko
 * Date: 3/1/2017
 * Time: 10:31 AM
 */

namespace App\SearchEngine;

use Elasticsearch;
use Validator;
use App\SearchEngine\SearchHelpers;

class Cache {
    public $cacheID;
    public $cacheMessage = '<h3 style="color:red">This is a copy of :url:. It was last updated on :timestamp:</h3>:html:';

    public function __construct($cacheID) {
        $this->cacheID = $cacheID;
    }

    public function validateID() {
        $validate = [
          'id' => $this->cacheID,
        ];

        $validator = Validator::make($validate, [
            'id' => 'required|max:32',
        ]);

        if($validator->fails()) return false;
        return true;
    }

    public function getCachedPage() {
        $params = [
            'index' => 'tsearch_sites_cache',
            'type' => 'site',
            'id' => $this->cacheID,
            'client' => [
              'ignore' => 404,
            ],
        ];

        $helpers = new SearchHelpers();
        $message = $this->cacheMessage;
        $return = Elasticsearch::get($params);
        $data = [];
        $data['id'] = $this->cacheID;
        if($return['found']) {
            $message = str_replace(":url:", $return['_source']['url'], $message);
            $message = str_replace(":timestamp:", $helpers->convertDateToReadable($return['_source']['timestamp']), $message);
            $message = str_replace(":html:", $return['_source']['html'], $message);
            $data['html'] = $message;
        }
        return $data;
    }
}