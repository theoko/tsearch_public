<?php
/**
 * Created by PhpStorm.
 * User: theoko
 * Date: 1/30/2017
 * Time: 2:34 PM
 */

namespace App\SearchEngine;

use App\SearchEngine\SearchHelpers;
use Elasticsearch;
use Validator;
use Illuminate\Http\Request;

class Search {
    public $query;
    public $page = 1;
    public $firstPage = 1;
    public $totalPages;
    public $limit = 5;
    public $pageButtons;
    public $noResultsMessage = '
    <h5>No results were found. You can try the following:</h5>
    <br>
    <ul>
        <blackquote>
            <li><h5>Try a more specific search</h5></li>
            <li><h5><a href="/submit">Submit</a> a website</h5></li>
        </blackquote>
    </ul>
    ';

    public function __construct($query) {
        $this->query = $query;
        $this->indexes = [
            'tsearch_sites',
            'tsearch_sites_cache',
            'tsearch_links',
        ];
    }

    public function validateQuery() {
        $validate = [
          'q' => $this->query,
        ];
        $validator = Validator::make($validate, [
            'q' => 'required|max:255',
        ]);

        if($validator->fails()) return false;
        return true;
    }

    public function validatePage(Request $request) {
        if($request->has('page')) {
            $validate = [
                'page' => $request->input('page'),
            ];
            $validator = Validator::make($validate, [
                'page' => 'min:1|max:15|integer',
            ]);
            if ($validator->fails()) return false;
            $this->page = $request->input('page');
        }
        return true;
    }

    public function indexSearch($from) {
        $data = [];
        $helpers = new SearchHelpers();

        $params = [
          'index' => 'tsearch_sites',
            'type' => 'site',
            'body' => [
              'query' => [
                  'bool' => [
                      'should' => [
                          'match' => [
                              'title' => $this->query,
                          ],
                          'match' => [
                              'keywords' => $this->query,
                          ],
                          'match' => [
                              'anchor' => $this->query,
                          ],
                          'match' => [
                              'url' => $this->query,
                          ],
                          'match' => [
                              'description' => $this->query,
                          ],
                          'match' => [
                              'text' => $this->query,
                          ],
                      ],
                      'minimum_should_match' => 4,
                  ],
              ],
            ],
            'size' => $this->limit,
            'from' => $from,
        ];

        $return = Elasticsearch::search($params);
        $data['query'] = $this->query;
        $data['hits'] = $return['hits']['total'];
        $data['took'] = $helpers->convertToSeconds($return['took']);
        $data['noResultsMessage'] = $this->noResultsMessage;
        if($return['hits']['total'] == 0) return $data;
        $data['results'] = [];
        $i = 0;
        foreach($return['hits']['hits'] as $h) {
            $h['_source']['title'] = $helpers->getTitle($h['_source']);
            $h['_source']['timestamp'] = $helpers->convertDateToReadable($h['_source']['timestamp']);
            $h['_source']['description'] = $helpers->getDescription($h['_source'], $this->query);
            $h['_source']['cacheID'] = $h['_id'];
            $data['results'][$i] = $h['_source'];
            $i++;
        }
        if(isset($data['results'][0]['referer'])) {
//            $referer = $data['results'][0]['referer'];
            $url = $data['results'][0]['url'];
            $f = new Filter();
            $res = $f->getRowPages($url, $this->query);
/*
*           Check against the referer
*           if(empty($res)) $res = $f->getRowPages($referer); else $data['results'][0]['htmlTable'] = $res;
*/
            if(empty($res)) $res = $f->getRowPages($url); else $data['results'][0]['htmlTable'] = $res;
            if(!empty($res)) $data['results'][0]['htmlTable'] = $res;
        }

        return $data;
    }

    public function generatePageButtons() {
        $links = [];

        $links['previous'] = ($this->page > 1) ? '<li class="waves-effect"><a href="' . url()->route('search', ['q' => $this->query, 'page' => ($this->page - 1)]) . '" title="Previous page"><i class="material-icons">chevron_left</i></a></li>' : '<li class="disabled"><a href="#"><i class="material-icons">chevron_left</i></a></li>';

        $links['next'] = ($this->page < $this->totalPages) ? '<li class="waves-effect"><a href="' . url()->route('search', ['q' => $this->query, 'page' => ($this->page + 1)]) .'" title="Next page"><i class="material-icons">chevron_right</i></a></li>' : '<li class="disabled"><a href="#"><i class="material-icons">chevron_right</i></a></li>';

        return $links;
    }

    public function generatePaginationInterface() {
        $buttons = $this->generatePageButtons();
        $pagination = '<ul class="pagination"><p>' . $buttons['previous'] . ' Page '. $this->page . ' of ' . $this->totalPages . ' pages '. $buttons['next'] . '</p></ul>';
        return $pagination;
    }

    public function getResults() {
        $offset = ($this->page - 1)  * $this->limit;
        $data = $this->indexSearch($offset);
        $total = $data['hits'];
        $pages = ceil($total / $this->limit);
        $this->totalPages = $pages;
        $data['pagination'] = $this->generatePaginationInterface();

        return $data;
    }

    public function textSearch() {
        $data = [];
        $params = [
            'index' => 'tsearch_sites',
            'type' => 'site',
            'body' => [
                'query' => [
                    'match' => [
                        'text' => $this->query,
                    ],
                ],
            ],
        ];

        $return = Elasticsearch::search($params);
        if($return['hits']['total'] == 0) return $data;
        $data = $return;

        return $data;
    }
}
