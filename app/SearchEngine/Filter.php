<?php

namespace App\SearchEngine;

use Elasticsearch;
use App\SearchEngine\SearchHelpers;

class Filter
{
    // set max results limit
    public $limit = 4;

    /*
     * gets row crawled pages by "referer"
     */
    public function getRowPages($referer, $query = null) {
        $data = [];
        $helpers = new SearchHelpers();

        $params = [
            'index' => 'tsearch_sites',
            'type' => 'site',
            'body' => [
                'query' => [
                    'bool' => [
                      'must' => [
                        'match' => [
                          'referer' => $referer,
                        ],
                      ],
                    ],
                ],
                'fields' => [
                  'title', 'url', 'timestamp',
                ],
            ],
            'size' => $this->limit,
        ];

        if($query != null ) {
            $params = [
                'index' => 'tsearch_sites',
                'type' => 'site',
                'body' => [
                    'query' => [
                        'bool' => [
                          'must' => [
                              'match' => [
                                  'referer' => $referer,
                              ],
                          ],
                            'should' => [
                                'match' => [
                                    'title' => $query,
                                ],
                            ],
                        ],
                    ],
                    'fields' => [
                        'title', 'url', 'timestamp',
                    ],
                ],
                'size' => $this->limit,
            ];
        }

        $return = Elasticsearch::search($params);
        if($return['hits']['total'] == 0) return $data;
        $data['results'] = [];
        $i = 0;
        foreach($return['hits']['hits'] as $h) {
            $h['fields']['timestamp'][0] = $helpers->convertDateToReadable($h['fields']['timestamp'][0]);
            $data['results'][$i] = $h['fields'];
            $i++;
        }

        return $helpers->toHtmlTable($data);
    }
}