<?php
/**
 * Created by PhpStorm.
 * User: theoko
 * Date: 1/30/2017
 * Time: 3:19 PM
 */

namespace App\SearchEngine;

use Elasticsearch;

class SearchHelpers {
    public $noDescriptionMessage = "No description retrieved for this result. The robots.txt file of this server might have prevented our bot from crawling this page.";
    public $noTitleMessage = "No title retrieved for this result. The robots.txt file of this server might have prevented our bot from crawling this page.";
    public $anchorSeperator = ' | ';
    public $htmlTable = '
    <table class="responsive-table">
        <tbody>
            :content:
        </tbody>
    </table>
    ';

    public function getTitle($d) {
        if(!empty($d['title'])) {
            if(isset($d['anchor'])) return $d['title'].$this->anchorSeperator.$d['anchor'];
            return $d['title'];
        } else {
            return $d['url'];
        }
    }

    public function getDescription($d, $t) {
        if(!empty(trim($d['description']))) {
            return $d['description'];
        } else {
            if(!empty(trim($d['text']))) return $this->trimDescriptionToTerm($d['text'], $t); else return $this->noDescriptionMessage;
        }
    }

    public function trimDescriptionToTerm($description, $query) {
        $trim = substr($description, 0, strpos($query, '.', strpos($query, '.')+1));
        $trim = $this->highlight($trim, $query);

        return $trim;
    }

    /*
     * converts array to an html table
     */
    public function toHtmlTable(array $data) {
        $data = array_chunk($data, 2, true);
        $row = "";
        $row .= "<tr>";
        foreach($data as $r) {
            foreach($r as $a) {
                $row .= "<td>";
                    foreach($a as $k => $d) {
                        $url = $d['url'][0];
                        $title = isset($d['title'][0]) ? $d['title'][0] : $this->noTitleMessage;
                        $time = $d['timestamp'][0];

                        $row .= '
                              <a href="' . $url . '">' . $title . '</a>
                              <p class="green-text">' . $url . '</p>
                              <p class="grey-text lighten-1">' . $time . '</p><br>
                        ';

                    }
                $row .= "</td>";
            }
        }
        $row .= "</tr>";

        $table = $this->htmlTable;
        $table = str_replace(":content:", $row, $table);
        return $table;
    }

    /*
     * Converts YYYY-MM-DD to human readable
     */
    public function convertDateToReadable($date) {
        return date("F jS, Y", strtotime($date));
    }

    public function convertToSeconds($time) {
        return $time / 1000;
    }

    public function highlight($text, $words) {
        preg_match_all('~\w+~', $words, $m);
        if(!$m)
            return $text;
        $re = '~\\b(' . implode('|', $m[0]) . ')\\b~i';
        return preg_replace($re, '<b>$0</b>', $text);
    }
}